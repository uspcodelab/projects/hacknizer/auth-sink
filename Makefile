REPO=jooaodanieel
PRFX=hacknizer-
TAG=latest
IMG=auth-sink


.PHONY: image push clean build run

image:
	docker image build . -t ${REPO}/${PRFX}${IMG}:${TAG}

push:
	docker push ${REPO}/${PRFX}${IMG}:${TAG}

clean:
	docker image rm ${REPO}/${PRFX}${IMG}:${TAG}

build:
	make image
	make push
	make clean

run:
	make image
	docker container run --rm \
		--name ${IMG} \
		${REPO}/${PRFX}${IMG}:${TAG}
