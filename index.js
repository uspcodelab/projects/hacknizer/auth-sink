const NATS = require('nats');
const { Client } = require('pg');

const nc = NATS.connect({
  url: process.env.NATS_URL,
  preserveBuffers: true
});


const JSONFromBuffer = (buf) => {
  const str = buf.toString();

  if (!str.endsWith('}'))
    str += '}';

  return JSON.parse(str);
};

const runQuery = async (qrText, values) => {
  const dbc = new Client({
    connectionString: process.env.DATABASE_URL
  });

  try {
    await dbc.connect();

    const res = await dbc.query(qrText, values);
    console.log(`--QUERY SUCCESS--\t|| ${res}`);
  } catch (err) {
    console.log(`--QUERY ERROR--\t|| ${err.stack}`);
  } finally {
    await dbc.end();
  }
};

nc.subscribe("new_user", async (msg) => {
  const qrText = `
    INSERT INTO "Users"
    (id, name, email, password)
    VALUES ($1, $2, $3, $4)
    RETURNING *
  `;

  const obj = JSONFromBuffer(msg);

  const values = [
    obj.id,
    obj.name,
    obj.email,
    obj.password
  ];

  await runQuery(qrText, values);  
});

console.log("SINK running");
